<?php
    //aqui se crea el objeto usuario
    class Validation {
        //atributos del objeto
        protected $_errors;
        protected $_validate;

        public function __construct(){
            $this->_validate = true;
            $this->_errors = array();
        }

        public function getValidation(){
            return $this->_validate;
        }
    
        public function getErrors(){
            return get_object_vars($this);
        }

        public function isEmpty($value, $input_id, $input_label){

            if (empty($value)){

                array_push($this->_errors, array(
                    'message' => "Complete el campo " . $input_label,
                    'id'=> $input_id
                ));

                $this->_validate = false;
            }
        }

        public function minlength($value, $input_id, $input_label, $min_length){

            if (strlen($value) < $min_length ){

                array_push($this->_errors, array(
                    'message' => "El campo " .  $input_label .  " debe tener un mínimo de " . $min_length,
                    'id'=> $input_id
                ));

                $this->_validate = false;
            }
        }

        public function maxlength($value, $input_id, $input_label, $max_length){

            if (strlen($value) < $max_length ){

                array_push($this->_errors, array(
                    'message' => "El campo " .  $input_label .  " debe tener un maximo de " . $max_length,
                    'id'=> $input_id
                ));

                $this->_validate = false;
            }
        }

        public function isEmail($email, $input_id){

            if(!filter_var($email, FILTER_VALIDATE_EMAIL)){

                array_push($this->_errors, array(
                    'message' => "Debe escribir un email válido",
                    'id' => $input_id
                ));

                $this->_validate = false;
            }
        }
    }

?>