<!doctype html>

<html lang="es">
    
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>ALÓ≈MALLORCA</title>
        <meta name="description" content="Es una pagina Web de viajes">
        <meta name="keywords" content="Mallorca viajes playas">
        <meta name="author" content="Manuel Valero Cabrera">
        
        <!-- Icono de la pestaña -->
        <link rel="shortcut icon" href="img/tab.ico">
        <!-- Font from Google -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Bitter">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
        <!-- Font Awesome -->   
        <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js" integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe" crossorigin="anonymous"></script>
        <!-- CSS -->
        <link rel="stylesheet" href="public/css/style.css">
    </head>