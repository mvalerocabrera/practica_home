		<!--Search: background + form-->
		<div class="row"><!--DIVISION 12 PARTS-->
			<div class="search col-xl-12">
				<div class="offset-xl-3 col-xl-6">
					<form>
						<div class="form-search form-group">
							<label for="exampleInputEmail1"><h2>Encuentra tu lugar magico en MALLORCA:</h2></label>
							<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Prueba con <<Playa de Es Trenc>>">
						</div>
						<button type="submit" class="btn btn-primary">Buscar</button>
					</form>
				</div>
			</div>
		</div>
		
		<!-- rounded images-->
		<div class="featured-container row" align="center">
			<div class="featured-article col-xl-4">
				<img src="img/catedral.jpg" alt="Catedral de Palma">
				<p>La Catedral-Basílica de Santa María de Palma de Mallorca también llamada Catedral de Mallorca es el principal edificio religioso de la isla de Mallorca. En mallorquín, se la conoce como La Seu (Seu o Seo es el nombre que reciben las catedrales en la Corona de Aragón). Consiste en un templo de estilo gótico levantino construido a la orilla de la bahía de Palma...<a href="https://es.wikipedia.org/wiki/Catedral_de_Santa_Mar%C3%ADa_de_Palma_de_Mallorca" title="Wikipedia" target="_blank"> ver más</a></p>
			</div>
			<div class="featured-article col-xl-4">
				<img src="img/playa.jpg" alt="Puesta de Sol">
				<p>Una de las cosas más bonitas del verano es quedarse en la playa con los amigos hasta que caiga la tarde o disfrutar de una pequeña excursión para ver el espectáculo de luces que ofrecen las puestas de sol de Mallorca. La isla tiene muchos lugares preciosos donde poder contemplar este espectáculo, desde miradores hasta playas...<a href="http://ocio.diariodemallorca.es/planes/noticias/nws-522299-10-lugares-donde-contemplar-puesta-sol-mallorca.html" title="Ociomallorca" target="_blank"> ver más</a><br>
			</div>
			<div class="featured-article col-xl-4">
				<img src="img/trenc.jpg" alt="Es Trenc">
				<p>Es Carbó también conocida como Platja d'es Carbó. Platja d'es Carbó está a 0,9 kilómetros de Colònia de Sant Jordi, frente al islote de na Molina, siendo el arenal sin urbanizar más frecuentado por los vecinos y veraneantes de este núcleo residencial turístico, aunque se deba llegar a pie desde Colònia de Sant Jordi...<a href="http://www.platgesdebalears.com/caplaibFront/playa_basico.es.jsp?cPla=64002&cMun=2&cIsl=MA" title="playes balears" target="_blank"> ver más</a></p>
			</div>
		</div>
		
		<!--Slider-->
		<div class="slider" align="center">
			<div class="col-xl-12">
			<h1>GASTRONOMÍA</h1>
			</div>
			<div id="carouselExampleIndicators" class="row carousel col-xl-4" data-ride="carousel">
							
				<ol class="carousel-indicators">
					<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
				</ol>
				
				<div class="carousel-inner">			
					<div class="carousel-item">
						<img src="img/sobrasada.jpg" alt="sobrasada">
					</div>
					
					<div class="carousel-item">
						<img src="img/cochinillo.jpg" alt="cochinillo">
					</div>

					<div class="carousel-item active">
							<img src="img/ensaimada.jpg" alt="ensaimada">
					</div>
				</div>
				
				<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				
				<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
			
				<div class="slider-article col-xl-8">
					<p>Cuando realizamos un viaje de placer son muchos los estímulos que buscamos. Conocer nuevas culturas, visitar paisajes de ensueño o encontrarnos con espectaculares rincones que ni siquiera sabíamos que podían ser reales. Existen muchas formas de disfrutar de unas vacaciones y muchos momentos que no olvidaremos, pero es sin duda, la hora de la comida (y los desayunos, y las cenas…) la que nos presenta un doble atractivo pues, además de ayudarnos a reponer fuerzas después de nuestros intensos momentos viajeros, nos servirán para seguir conociendo la cultura del lugar, a partir de sus platos autóctonos y de su especial cocina local.<br><br>
						Mallorca, además de tener un ambiente espectacular y unos lugares paradisíacos pondrá al alcance de sus turistas una oferta gastronómica de calidad, sabrosa y saludable. No dudes en revisar los platos típicos de la zona que encontrarás a continuación, pues seguro que no te dejarán indiferente. <a href="https://www.tripadvisor.es/Restaurants-g187462-Majorca_Balearic_Islands.html" title="Tripadvisor" target="_blank"> Prepara tu tenedor…</a></p>
				</div>
		</div>