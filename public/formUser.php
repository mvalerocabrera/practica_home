<?php include "header.php";?>
<?php include "headboard.php";?>

<body>
    
    <div class="col-xl-12 title-search">
        <h3>Formulario de Registro</h3>
    </div>

    <div class="errors-container hidden">
        <ul class="errors"></ul>
    </div>

    <div class="col-xl-12 success-container hidden">
        <h1 class="success"></h1>
    </div>

    <div class="container formulario">

        <div class="offset-xl-4 col-xl-4 formulario">		

            <form id="contact-form" class="admin-form" action="app/requests/UserRequest.php">
                <label id="messages"></label> 
                <br>Nombre:<br>
                <input type="text" id="name" name="name"><br><br>
                <p id="name-error" class="input-error"></p>
                <label id="messages"></label> 
                Apellido:<br>
                <input type="text" id="lastname" name="lastname"><br><br>
                <p id="lastname-error" class="input-error"></p>
                <label id="messages"></label>
                Correo Electronico:<br>
                <input type="text" id="email" name="email"><br><br>
                <p id="email-error" class="input-error"></p>
                <label id="messages"></label>
                Contraseña:<br>
                <input type="password" id="" name="password"><br><br>
                <p id="password-error" class="input-error"></p>

                <input type="submit" id="enviar" value="Enviar">
            </form>

        </div>

    </div>

<?php include "footer.php";?>
<?php include "scripts.php";?>