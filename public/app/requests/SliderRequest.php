<?php

    require_once "SliderController.php";
    require_once "Validation.php";

    $validation = new Validation();
    $validation->isEmpty($_FILES['image']['name'], 'image', 'enviar un archivo');
    $validation->fileType($_FILES['image']['name'], 'image', array( 'jpeg', 'jpg', 'png'),'images');
    $validation->maxFileSize($_FILES['image']['size'], 'image', 200000,'image');

    if ($validation->getValidation()){ 

        $slider = new SliderController($_POST, $_FILES['image']); //creo el objeto y lo meto en la variable slider
        $response['_validation'] = $validation->getValidation();
        $response['message'] = $slider->getSliderTitle();
        echo json_encode($response); //estamos utilizando el metodo getUsertitle si esta validado podremos ver el nombre y el apellido
    
    }else{

        echo json_encode($validation->getErrors()); //devolvemos a ajax (con echo)

    }
    
?>