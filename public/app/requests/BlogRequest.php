<?php

    require_once "BlogController.php";
    require_once "Validation.php";

    $validation = new Validation(); //crea una copia de la clase validation (un objeto) para rellenarlo con esto
    $validation->isEmpty($_POST['title'], 'title', 'titulo');
    $validation->isEmpty($_POST['comment'], 'comment','comentario');
    $validation->isEmpty($_POST['category'], 'category', 'categoria');
    $validation->minLength($_POST['title'], 2, 'title', 'titulo');
    $validation->maxLength($_POST['title'], 21, 'title', 'titulo');
    $validation->minLength($_POST['comment'], 2, 'comment', 'comentario');
    $validation->maxLength($_POST['comment'], 100, 'comment', 'comentario');

    if($validation->getValidation()){

        $Blog = new BlogController($_POST);
        $response['_validation'] = $validation->getValidation();//creamos un array response con una key validation de lo que hay
        $response['message'] = $Blog->getBlogTitle();
        echo json_encode($response);

    }else{
        echo json_encode($validation->getErrors());
    }
?>
